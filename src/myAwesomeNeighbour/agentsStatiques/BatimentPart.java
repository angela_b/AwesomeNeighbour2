package myAwesomeNeighbour.agentsStatiques;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.TrainsSupervisor;
import myAwesomeNeighbour.agentsMobiles.Pieton;
import myAwesomeNeighbour.agentsMobiles.Wagon;
import myAwesomeNeighbour.geometrie.Point;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.watcher.Watch;
import repast.simphony.engine.watcher.WatcherTriggerSchedule;
import repast.simphony.space.grid.Grid;

public class BatimentPart extends Agent{

	public enum BatimentPartType {
		CENTER,
		BORDER
	}
	
	private Batiment batiment;
	private Point position;
	public BatimentPartType type;
	
	private boolean isPeopleEntrance = false;	
	
	public BatimentPart(Grid<Agent> grid, Point position, Batiment batiment, BatimentPartType type) {	
		super(grid);
		this.position = position;
		this.batiment = batiment;
		this.type = type;
	}
	
	public Batiment getBatiment() {
		return this.batiment;
	}
	public Point getPosition() { return this.position; }
	public void setPeopleEntrance(boolean isPeopleEntrance) { this.isPeopleEntrance = isPeopleEntrance; }
	public Boolean getPeopleEntrance() { return this.isPeopleEntrance;}

	@Watch(watcheeClassName = "myAwesomeNeighbour.agentsMobiles.Pieton", 
		   watcheeFieldNames = "destiner", 
		   query = "linked_from",
		   whenToTrigger = WatcherTriggerSchedule.LATER)
	public void pedestrianArrived(Pieton person) {
		this.batiment.timesVisited += 1;
		double actualTick = RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		System.out.println("[INFO]" + actualTick + "] Une personne est arrivée à destination: " + person);
		this.batiment.addPerson(person);
	}
	
	@Watch(watcheeClassName = "myAwesomeNeighbour.agentsMobiles.Wagon",
		   watcheeFieldNames = "nextStation",
		   query = "linked_from",
		   whenToTrigger = WatcherTriggerSchedule.IMMEDIATE,
		   scheduleTriggerPriority = 5)
	public void trainArrived(Wagon wagon) {
		double actualTick = RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		System.out.println("[INFO][" + actualTick + "] Un train est arrivé en station: " + this.position);
		Gare gare = (Gare)this.batiment;
		wagon.train.removeTravelers(gare);
		wagon.train.getPeopleFromGare(gare);
	}
	
	@Watch(watcheeClassName = "myAwesomeNeighbour.agentsMobiles.Pieton",
			watcheeFieldNames = "stationStep",
			query = "linked_from",
			whenToTrigger = WatcherTriggerSchedule.IMMEDIATE,
			scheduleTriggerPriority = 1)
	public void pedestrianArrivedToStation(Pieton person) {
		
		if (!(this.batiment instanceof Gare)) { return; }
		double actualTick = RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		this.batiment.timesVisited += 1;
		if (person.isWaitingForTrain) { // Cas où la person sort d'un train
			System.out.println("[INFO][" + actualTick + "] Une personne sort de son train: " + this.position);
			person.isWaitingForTrain = false;
		} else { // Cas où la personne attend son train
			System.out.println("[INFO][" + actualTick + "] Une personne attend son train: " + this.position);
			person.isWaitingForTrain = true;
			Gare gare = (Gare) this.batiment;
			gare.addPassenger(person, TrainsSupervisor.getInstance().getFasterTrainAccedingToStation(gare, (Gare)person.stationStep.batiment));
		}
		
	}
}
