package myAwesomeNeighbour.agentsStatiques;

import java.util.ArrayList;
import java.util.HashMap;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.agentsMobiles.Pieton;
import myAwesomeNeighbour.agentsMobiles.Train;
import myAwesomeNeighbour.geometrie.Point;
import repast.simphony.space.grid.Grid;

public class Gare extends Batiment{

	private HashMap<Train, ArrayList<Pieton>> waitingPeople = new HashMap<Train, ArrayList<Pieton>>();
	
	/** 
	 * @param grid La grille de jeu
	 * @param positions La position des blocs de la gare 
	 * @param peopleEntrances La liste des entrées/sorties de la gare 
	 */
	public Gare(Grid<Agent> grid, ArrayList<Point> positions, ArrayList<Point> peopleEntrances) {
		super(grid, positions, peopleEntrances);
		/** Generate train paths **/
	}

	public void addPassenger(Pieton passenger, Train train) {
		if (!this.waitingPeople.containsKey(train)) { this.waitingPeople.put(train, new ArrayList<>()); }
		this.waitingPeople.get(train).add(passenger);
	}
	
	public ArrayList<Pieton> getPassengers(Train train) {
		if (!this.waitingPeople.containsKey(train)) { this.waitingPeople.put(train, new ArrayList<>()); }
		ArrayList<Pieton> passengers = new ArrayList<>(this.waitingPeople.get(train));
		this.waitingPeople.get(train).clear();
		return passengers;
	}
	
	@Override
	public void step() { }

}
