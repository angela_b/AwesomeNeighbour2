package myAwesomeNeighbour.agentsMobiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;
import java.util.stream.Collectors;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.ContextCreator.Constants;
import myAwesomeNeighbour.PlaningGenerator;
import myAwesomeNeighbour.TrainsSupervisor;
import myAwesomeNeighbour.agentsStatiques.Batiment;
import myAwesomeNeighbour.agentsStatiques.BatimentPart;
import myAwesomeNeighbour.agentsStatiques.Chemin;
import myAwesomeNeighbour.agentsStatiques.Gare;
import myAwesomeNeighbour.agentsStatiques.Vide;
import myAwesomeNeighbour.agentsStatiques.Chemin.CheminType;
import myAwesomeNeighbour.geometrie.Point;
import myAwesomeNeighbour.geometrie.PointWeight;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;

/** Classe definissant les agents mobiles de base, les piétons, sur la grille **/
public class Pieton extends AgentMobile {
	/** Nombre de tic depuis la naissance **/
	// private int age = 0;
	/** Nombre de tic pour schédule le vieillissement et les déplacements **/
	// private int tic = 0;
	/** La Voiture possédé par le pieton **/
	private Point position;
	private Voiture ownedCar = null;
	private Stack<Batiment> planning = new Stack<>();
	public BatimentPart destiner = null;
	public BatimentPart stationStep = null;
	private Stack<BatimentPart> stationsSteps = new Stack<>();
	public boolean isWaitingForTrain = false;

	/** Constructeur **/
	public Pieton(Grid<Agent> grid) {
		super(grid);
		System.out.println("[INFO] Un nouvel habitant est apparu.");
	}

	public void setPosition(Point position) { this.position = position; }
	public Point getPosition() { return this.position; }
	
	public void transferEdgesTo(Context<Object> context, Agent agent) {
		Network<Agent> net = (Network<Agent>)context. getProjection("agent network");
		net.addEdge(agent, this.destiner);
		if (this.stationStep != null) { net.addEdge(agent, this.stationStep); }
	}
	
	public Point getGoal() {
		if (this.planning.isEmpty()) { this.planning = PlaningGenerator.getInstance().generatePlaning(Constants.PLANNING_SIZE, this); }
		Batiment nextBatiment = this.planning.pop();
		Point goal = nextBatiment.getRandomEntryPosition();
		if (goal.equalsTo(this.getPosition())) { return this.getGoal(); }
		return goal;
	}

	@Override
	@ScheduledMethod(start = 1, interval = Constants.PENALITY_PEDESTRIAN, priority = Constants.SCHEDULE_PEDESTRIAN_PRIORITY)
	public void step() {

		/** Si le piéton attend son train, il est immobile **/
		if (this.isWaitingForTrain) { return; };
		
		/** Récupération de la position du piéton pour traitement  */
		Context<Object> context = ContextUtils.getContext(this); 
		Network<Agent> net = (Network<Agent>)context. getProjection("agent network");

		
		/** Si le chemin n'a pas encore été définit, on le génère **/
		if (this.listMoves.isEmpty()) {
			Point goal = this.getGoal();
			this.listMoves = this.PCC(goal);

			/** Append the stationsSteps stack **/
			for (Point position : this.listMoves) {
				for (Agent agent : this.grid.getObjectsAt(position.getX(), position.getY())) {
					if (agent instanceof BatimentPart && ((BatimentPart)agent).getBatiment() instanceof Gare) {
						BatimentPart part = ((BatimentPart)agent);
						Gare gare = ((Gare)part.getBatiment());
						if (TrainsSupervisor.getInstance().getIntersectionsBetweenStationAndPaths(gare).contains(part.getPosition())) {
							if (!this.stationsSteps.isEmpty()) {
								BatimentPart nextPart = this.stationsSteps.peek();
								if (!TrainsSupervisor.getInstance().arePositionsFromSamePath(gare, part.getPosition(), nextPart.getPosition())) // Supprime les "doublons" : une gare peut être intersecter plusieur rails d'un trainPath
									this.stationsSteps.push(part);
							} else {
								this.stationsSteps.push((BatimentPart) agent);
							}
							break;
						}
					}
				}
			}
			
			for (Agent possibleDestiner : grid.getObjectsAt(goal.getX(), goal.getY())) {
				if (possibleDestiner instanceof BatimentPart) { 
					this.destiner = (BatimentPart) possibleDestiner;
					break;
				}
			}
			
			/** Generer la fleche vers la destination **/
			net.addEdge(this, this.destiner);	
			
			/** Generer la fleche vers la prochaine gare **/
			if (!this.stationsSteps.isEmpty()) {
				this.stationStep = this.stationsSteps.pop();
				net.addEdge(this, this.stationStep);
			}
		}	

		/** Récupération sur la stack du move a faire **/
		Point dest = this.listMoves.pop();
		
		/** Si la position suivante est une route, on transforme le pieton en voiture. Sinon, on le déplace à la position suivante **/
		if (this.transformedIntoCar(dest)) {
			this.transferEdgesTo(context, this.ownedCar);
			this.grid.moveTo(this.ownedCar, dest.getX(), dest.getY());
		} else {
			this.grid.moveTo(this, dest.getX(), dest.getY());
			this.position = dest;
			if (this.stationStep != null && this.position.equalsTo(this.stationStep.getPosition())) {
				this.refreshNextStation(net);
			}
		}
		
		if (this.listMoves.isEmpty()) {
			this.destiner = null;
		}
	}
	
	public void refreshNextStation(Network<Agent> net) {
		BatimentPart previousStationStep = this.stationStep;
		this.stationStep = this.stationsSteps.isEmpty() ? null : this.stationsSteps.pop();
		net.removeEdge(net.getEdge(this, previousStationStep));
		if (this.stationStep != null) { net.addEdge(this, this.stationStep); }
	}

	public boolean transformedIntoCar(Point position) {
		Context<Object> context = ContextUtils.getContext(this); 
		Iterable<Agent> objs2 = grid.getObjectsAt(position.getX(), position.getY());
		for (Agent agent : objs2) {
			if (agent instanceof Chemin && ((Chemin) agent).isRoad()) {
				// Transformations en voiture 
				context.remove(this);
				if (this.ownedCar == null) {
					Voiture v = new Voiture(grid);
					v.setMaster(this);
					this.ownedCar = v;
				}

				this.ownedCar.setListMoves(this.listMoves);
				context.add(this.ownedCar);
				return true;
			}
		}
		return false;
	}

	public Voiture getMyCar() {
		return this.ownedCar;
	}

	public void setMyCar(Voiture car) {
		this.ownedCar = car;
	}

	/** -------------------- Path finding part -------------------- **/

	/** Fonction auxiliaire à la recherche de plus court chemin. Vérifie qu'une position est accessible ou non **/
	private boolean isAccessible(Point from, Point to) { 
		Iterable<Agent> listFrom = this.grid.getObjectsAt(from.getX(), from.getY());
		Iterable<Agent> listTo = this.grid.getObjectsAt(to.getX(), to.getY());

		for (Agent agentTo : listTo) {

			/** Si il n'y a aucun agent sur une case, le piéton n'y accèdera pas également 
			 ** Le but étant de forcer les piétons à se déplacer sur un chemin uniquement.
			 */
			if (agentTo instanceof Vide) { return false; }

			/** Les bâtiments ne sont accessibles qu'en passant par les entrées ou par les batiments **/
			/** En particulier, les gares sont également accessibles par des rails **/
			if (agentTo instanceof BatimentPart) {
				if (((BatimentPart) agentTo).getBatiment().getEntrancesPositions().contains(to)) { return true; }
				List<Point> batimentPositions = ((BatimentPart) agentTo).getBatiment().getPositions();
				if (batimentPositions.contains(to)) {
					if (batimentPositions.contains(from)) { return true; }
					if (((BatimentPart) agentTo).getBatiment() instanceof Gare) {
						Gare gare = (Gare)((BatimentPart) agentTo).getBatiment();
						if (TrainsSupervisor.getInstance().getIntersectionsBetweenStationAndPaths(gare).contains(to)/*gare.containsIntersectionRail(to)*/) {
							for (ArrayList<Point> trainPath : TrainsSupervisor.getInstance().getPathsAccedingToStation(gare)) {
								if (trainPath.contains(from)) { 
									return true; };
							}
						}
					}
				}
				return false;
			}
			/** Les rails ne sont accessibles que si l'on provient d'une gare où que l'on provient d'un autre rail **/
			if (agentTo instanceof Chemin && ((Chemin) agentTo).getPathType() == CheminType.RAIL) {
				for (Agent agentFrom : listFrom) {
					if (agentFrom instanceof Chemin && ((Chemin) agentFrom).getPathType() == CheminType.RAIL) { return true; }
					if (agentFrom instanceof BatimentPart && ((BatimentPart) agentFrom).getBatiment() instanceof Gare) { 
						Gare gare = (Gare)(((BatimentPart) agentFrom).getBatiment());
						for (ArrayList<Point> trainPath : TrainsSupervisor.getInstance().getPathsAccedingToStation(gare)) {
							if (trainPath.contains(to)) { return true; }
						}
					}
				}
				return false;
			}
		}

		/** Tous les cas de déplacement dans un bâtiment ont été géré ci-dessus (entrée -> intérieur | intérieur -> entrée)
		 *  Les cas restant des bâtiments sont intérieur -> extérieur sans passer par l'entrée (non valide) et entrée -> extérieur (valide)
		 *  Il faut bien entendu ne pas autoriser ces cas.
		 *  De la même façon, les agents n'ont pas le droit de sortir d'un rail dans tous les autres cas.
		 */
		for (Agent agentFrom : listFrom) {
			if (agentFrom instanceof BatimentPart && !((BatimentPart) agentFrom).getBatiment().getEntrancesPositions().contains(from)) { return false; }
			if (agentFrom instanceof Chemin && ((Chemin) agentFrom).getPathType() == CheminType.RAIL) { return false; }
		}

		return true; 
	}

	/** Fonction auxiliaire à la recherche de plus court chemin. Appelée lorsqu'une position est accessible. 
	 ** Retourne le poids de cette position afin de gérer les priorités (il est par exemple plus rapide de prendre la voiture que de marcher
	 **/
	private int getCost(Point p) {
		Iterable<Agent> list = this.grid.getObjectsAt(p.getX(), p.getY());
		for (Agent agent : list) {
			if (agent instanceof Chemin) {
				switch (((Chemin)agent).getPathType()) {
				case RAIL:
					return Constants.PENALITY_TRAIN;
				case ROUTE:
					return Constants.PENALITY_CAR;
				case TROTTOIR:
					return Constants.PENALITY_PEDESTRIAN;
				}
			}	
			/** Il est possible que le chemin d'un piéton passe par un batiment (notamment les gares).
			 ** En réalité, le piéton ne marchera pas directement dans la gare, mais se fera "expulser" directement à une entrée du bâtiment à l'arrivée du train.
			 ** Cela ne coûte donc rien en terme de tick pour le piéton. Mais on choisit quand même de mettre un coût à ces blocs pour éviter que l'algo tourne en
			 * 	rond dans un batiment.
			 */
			if (agent instanceof BatimentPart) { return Constants.PENALITY_PEDESTRIAN; }
		}
		return Integer.MAX_VALUE; 
	}

	/** Algo de plus court chemin, rework de celui de Ben. Pour gérer les blocs inaccessibles et les coûts, il faut changer les fonctions ci-dessus :D **/
	public Stack<Point> PCC(Point dest) {
		/** Initialise la pile **/
		Stack<Point> path = new Stack<>();

		/** Initialise le tableau des poids **/
		int height = this.grid.getDimensions().getHeight();
		int width = this.grid.getDimensions().getWidth();
		Integer[][] weightGraph = new Integer[width][height];
		for (Integer[] line : weightGraph)
			Arrays.fill(line, Integer.MAX_VALUE);

		/** Initialise la priority queue et ajout de la position de départ**/
		Queue<PointWeight> queue = new PriorityQueue<PointWeight>((p1, p2) -> { return p1.weight - p2.weight; });

		/** On ajoute un coût de 0 pour la position de départ et on l'ajoute dans la priority queue **/
		GridPoint currentLocation = this.grid.getLocation(this);
		weightGraph[currentLocation.getY()][currentLocation.getX()] = 0;
		queue.add(new PointWeight(currentLocation.getX(), currentLocation.getY(), 0));


		while (!queue.isEmpty()) {			
			/** Pop l'élément avec le coût le plus faible **/
			PointWeight actualPoint = queue.poll();

			/** Si l'élément correspond a la destination, on arrête sans finir de parcourir la grille **/
			if (actualPoint.position.equalsTo(dest)) break;

			/** Récupère les voisins accessibles **/
			List<Point> accessibleNeighbours = 
					actualPoint.position.getNeighbours(this.grid.getDimensions().getWidth(), this.grid.getDimensions().getHeight())
					.stream() /** .stream() permet de faire du second ordre **/
					.filter(p -> weightGraph[p.getY()][p.getX()] == Integer.MAX_VALUE && this.isAccessible(actualPoint.position, p)) /** On s'assure que la position n'a jamais été visité, et qu'elle est accessible **/
					.collect(Collectors.toList());	/** On récupère les résultats collectés sous forme de liste **/

			/** Ajoute dans la queue les voisins accessibles **/
			for (Point p : accessibleNeighbours) {
				/** On s'assure en plus que le poids ne sera jamais plus grand que Integer.MAX_VALUE (c'est juste une sécu, en vrai ça ne se produirai jamais même sans) **/
				PointWeight pointWeight = new PointWeight(p, actualPoint.weight == Integer.MAX_VALUE ? Integer.MAX_VALUE : this.getCost(p) + actualPoint.weight);
				queue.add(pointWeight); 
				weightGraph[pointWeight.position.getY()][pointWeight.position.getX()] = pointWeight.weight;
			}
		}

		/** On détermine le chemin en le parcourant en sens inverse **/
		Point from = dest;
		Point to = new Point(currentLocation.getX(), currentLocation.getY());
		while (!from.equalsTo(to)) {
			path.add(from);
			/** Le .get à la fin de retournera jamais une erreur, car le minimum existe forcément.
			 *  S'il vient à en retourner une, c'est qu'il existe des endroits non accessible. **/
			Point fromCopy = new Point(from.getX(), from.getY());
			from = from.getNeighbours(width, height).stream()
					.filter(p -> this.isAccessible(fromCopy, p) && !path.contains(p)) // Supprime des égalités possiible avec le parcours inverse, et ainsi des cas non voulus
					.min((p1, p2) -> { return weightGraph[p1.getY()][p1.getX()] - weightGraph[p2.getY()][p2.getX()]; }).get();		
		}

		/** Dans le cas où l'on veut ajouter la position de départ au chemin (ie celle sur laquelle est positionné l'agent, décommenter la ligne ci-dessous : **/
		// 	path.add(to);

		return path;
	}

	/**  Affiche le graph des poids utilisé dans l'algo ci-dessus **/
	private void printGraph(Integer[][] graph, int height, int width) {

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (graph[i][j] == Integer.MAX_VALUE) System.out.print("   X ");
				else System.out.print(" " + (graph[i][j] < 100 ? " " : "") + (graph[i][j] < 10 && graph[i][j] >= 0 ? " " : "") + graph[i][j] + " ");
			}
			System.out.println("");
		}
	}


}
