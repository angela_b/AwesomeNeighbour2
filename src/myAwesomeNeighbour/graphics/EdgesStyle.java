package myAwesomeNeighbour.graphics;

import java.awt.Color;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.agentsMobiles.Pieton;
import myAwesomeNeighbour.agentsMobiles.Wagon;
import repast.simphony.space.graph.RepastEdge;
import repast.simphony.visualizationOGL2D.DefaultEdgeStyleOGL2D;

public class EdgesStyle extends DefaultEdgeStyleOGL2D {

	@Override
	public Color getColor(RepastEdge<?> edge) {	
		if ((Agent)edge.getSource() instanceof Wagon) {
			return Color.BLUE;
		}
		return Color.RED;
	}
	
}
