package myAwesomeNeighbour;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import cern.jet.random.Uniform;
import myAwesomeNeighbour.agentsMobiles.Train;
import myAwesomeNeighbour.agentsStatiques.Gare;
import myAwesomeNeighbour.geometrie.Point;

import repast.simphony.context.Context;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;

public class TrainsSupervisor {
	
	/** Singleton definition **/
	private static TrainsSupervisor INSTANCE = null;
	public static void clearInstance() { INSTANCE = null; }
	public static TrainsSupervisor getInstance() { 
		/* Lazy implementation */
		if (INSTANCE == null) { INSTANCE = new TrainsSupervisor(); }
		return INSTANCE; 
	}

	
	private ArrayList<ArrayList<Point>> allPaths = new ArrayList<>();
	private ArrayList<Gare> allStations = new ArrayList<>();
	private ArrayList<Train> allTrains = new ArrayList<>();
	
	private ArrayList<Pair<Integer, Integer>> associationsStationPath = new ArrayList<>();
	private ArrayList<Pair<Integer, Integer>> associationsTrainPath = new ArrayList<>();
	
	
	
	@Override
	public String toString() {
		return
				"paths: " + this.allPaths.toString() + "\n" +
				"stations: " + this.allStations.size() + "\n" +
				"stations - paths: " + this.associationsStationPath.toString();
	}
	
	
	/** A partir d'un chemin donné, créé un chemin bouclé **/
	private ArrayList<Point> loopPath(ArrayList<Point> path) {
		ArrayList<Point> loopedPath = new ArrayList<Point>();
		
		/** Here we're constructing the path as a loop **/		
		for (int i = 0; i < path.size(); i++) { loopedPath.add(path.get(i)); }
		if (!path.get(0).equalsTo(path.get(path.size() - 1))) {
			for (int i = path.size() - 2; i > 0; i--) { loopedPath.add(path.get(i)); }
		}
		
		return loopedPath;
	}
	
	/** Sauvegarde un chemin, et l'associe à toutes les gares par lesquelles il passe. **/
	public void savePath(ArrayList<Point> path) {
		ArrayList<Point> loopedPath = this.loopPath(path);
		this.allPaths.add(loopedPath);
		int pathIndex = this.allPaths.size() - 1;
		for (int stationIndex = 0; stationIndex < this.allStations.size(); stationIndex++) {
			if (!this.allStations.get(stationIndex).getPositions().stream().noneMatch(p -> path.contains(p))) {
				this.associationsStationPath.add(Pair.of(stationIndex, pathIndex));
			}
		}
	}
	
	/** Sauvegarde une gare, et l'associe à tous les chemins qui la croisent. **/
	public void saveStation(Gare station) { 
		this.allStations.add(station);
		int stationIndex = this.allStations.size() - 1;
		for (int pathIndex = 0; pathIndex < this.allPaths.size(); pathIndex++) {
			if (!this.allPaths.get(pathIndex).stream().noneMatch(p -> station.getPositions().contains(p))) {
				this.associationsStationPath.add(Pair.of(stationIndex, pathIndex));
			}
		}
	}
	
	/** Génère aléatoirement numberPerPath trains par chemin **/
	public void generateTrains(Context<Agent> context, Grid<Agent> grid, int numberPerPath) {
		
		for (int pathIndex = 0; pathIndex < this.allPaths.size(); pathIndex++) {
			for (int n = 0; n < numberPerPath; n++) {
				ArrayList<Point> personalisedPath = new ArrayList<>();
				Uniform u = RandomHelper.createUniform();
				int headPosition = u.nextIntFromTo(0, this.allPaths.get(pathIndex).size() - 1);
				boolean direction = u.nextBoolean();
			
				/** Construction du path **/
				for (int k = headPosition; (direction ? (k >= 0) : (k < this.allPaths.get(pathIndex).size())); k = direction ? k - 1 : k + 1)
					personalisedPath.add(this.allPaths.get(pathIndex).get(k));
				for (int k = direction ? this.allPaths.get(pathIndex).size() - 1 : 0; k != headPosition; k = direction ? k - 1 : k + 1)
					personalisedPath.add(this.allPaths.get(pathIndex).get(k));
			
				Train train = new Train(grid, personalisedPath, u.nextIntFromTo(2, 5));
				context.add(train);
				train.generateInContext(context); // Génère les wagons du train
				train.updateDirection(context);
			
			
				this.allTrains.add(train);
				this.associationsTrainPath.add(Pair.of(this.allTrains.size() - 1, pathIndex));
			}	
		}
	}
	
	
	/** Quelques getters pour simplifier le code */
	private ArrayList<Integer> getPathsIndexsAccedingToStation(Gare station) { 
		ArrayList<Integer> validePaths = new ArrayList<>();
		Integer gareIndex = this.allStations.indexOf(station);
		for (Pair<Integer, Integer> stationPathAssociation : this.associationsStationPath) {
			if (stationPathAssociation.getLeft() == gareIndex) {
				validePaths.add(stationPathAssociation.getRight());
			}
		}
		return validePaths;
	}
	public List<ArrayList<Point>> getPathsAccedingToStation(Gare station) {
		return this.getPathsIndexsAccedingToStation(station).stream().map(i -> new ArrayList<>(this.allPaths.get(i))).collect(Collectors.toList());
	}
	
	public ArrayList<Point> getIntersectionsBetweenStationAndPaths(Gare station) {
		ArrayList<Point> intersections = new ArrayList<>();
		System.out.println("Paths index: " + this.getPathsIndexsAccedingToStation(station));
		for (int pathIndex : this.getPathsIndexsAccedingToStation(station)) {
			for (Point p : this.allPaths.get(pathIndex)) {
				if (station.getPositions().contains(p)) {
					intersections.add(p);
				}
			}
		}
		return intersections;
	}
		
	/** Récupère la distance entre un train et une gare **/
	/** En supposant que le train et la gare partagent un même chemin **/
	private int getDistanceBetweenTrainAndStation(Train train, Gare station) {
		int distance = 0;
		ArrayList<Point> intersections = this.getIntersectionsBetweenStationAndPaths(station);
		for (Point p : train.path) {
			distance += 1;
			if (intersections.contains(p)) { break; }
		}
		return distance;
	}
	
	/** Récupère le train qui accèdera à une certaine gare le plus vite **/
	public Train getFasterTrainAccedingToStation(Gare fromStation, Gare toStation) {
		int fromStationIndex = this.allStations.indexOf(fromStation);
		int toStationIndex = this.allStations.indexOf(toStation);
		
		int fasterValue = Integer.MAX_VALUE;
		Train fasterTrain = null;
		
		ArrayList<Integer> availableTrains = new ArrayList<>();
		
		for (int i = 0; i < this.allPaths.size(); i++) {
			if (this.associationsStationPath.contains(Pair.of(fromStationIndex, i)) 
				&& this.associationsStationPath.contains(Pair.of(toStationIndex, i))) {
					for (Pair<Integer, Integer> p : this.associationsTrainPath) {
						if (p.getRight() == i) { availableTrains.add(p.getLeft()); }
					}
			}
		}
		
				
		for (Integer i : availableTrains) {
			int distance = 
					this.getDistanceBetweenTrainAndStation(this.allTrains.get(i), toStation) +
					this.getDistanceBetweenTrainAndStation(this.allTrains.get(i), fromStation);
			if (distance < fasterValue) {
				fasterTrain = this.allTrains.get(i);
				fasterValue = distance;
			}
		}
		
		return fasterTrain;
	}
	
	
	/** Indique si deux emplacements d'un même chemin appartiennent à la même gare **/
	public boolean arePositionsFromSamePathAndSameStation(Point pos1, Point pos2) { 
		if (this.allPaths.stream().noneMatch(path -> path.contains(pos1) && path.contains(pos2))) { return false; }
		if (this.allStations.stream().noneMatch(station -> station.getPositions().contains(pos1) && station.getPositions().contains(pos2))) { return false; }
		return true;
	}
	
	public boolean arePositionsFromSamePath(Gare station, Point pos1, Point pos2) {
		if (!(station.getPositions().contains(pos1) && station.getPositions().contains(pos2))) { return false; }
		if (this.getPathsIndexsAccedingToStation(station).stream().noneMatch(i -> this.allPaths.get(i).contains(pos1) && this.allPaths.get(i).contains(pos2))) {
			return false;
		}
		return true;
	}
	
}
