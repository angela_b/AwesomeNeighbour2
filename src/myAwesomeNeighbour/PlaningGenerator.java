package myAwesomeNeighbour;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

import myAwesomeNeighbour.agentsMobiles.Pieton;
import myAwesomeNeighbour.agentsStatiques.Batiment;

public class PlaningGenerator{
	private ArrayList<Batiment> batiments = new ArrayList<Batiment>();
	
	private PlaningGenerator() { }
	
	private static PlaningGenerator INSTANCE = null;	
	
	public static PlaningGenerator getInstance() { 
		/* Lazy implementation */
		if (INSTANCE == null) { INSTANCE = new PlaningGenerator(); }
		return INSTANCE; 
	}
	
	public static void clearInstance() { INSTANCE = null; }
	
	public void addBatiment(Batiment b) { this.batiments.add(b); }
	public void removeBatiment(Batiment b) { this.batiments.remove(b); }
	public ArrayList<Batiment> getBatimentsList() { return this.batiments; }

	public Stack<Batiment> generatePlaning(int nbBatiment, Pieton pieton) {
		 Random randomGen = new Random();
		 Stack<Batiment> subBatiments = new Stack<Batiment>();
		 if (nbBatiment >= this.batiments.size())
			 nbBatiment = this.batiments.size() - 1;
		 for (int i = 0; i < nbBatiment; ) {
			 int index = randomGen.nextInt(this.batiments.size());
			 if (!subBatiments.contains(this.batiments.get(index)) 
				  && !this.batiments.get(index).getEntrancesPositions().contains(pieton.getPosition()) 
				  && !this.batiments.get(index).containsPerson(pieton)) {
				 subBatiments.add(this.batiments.get(index));
				 i++;
			 }
		 }
		 return subBatiments;
	}	 
}
