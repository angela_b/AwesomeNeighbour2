package myAwesomeNeighbour.agentsStatiques;

import myAwesomeNeighbour.agentsMobiles.Pieton;
import myAwesomeNeighbour.geometrie.Point;

import java.util.ArrayList;

import myAwesomeNeighbour.Agent;
import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;

public class Appartement extends Batiment {

	protected ArrayList<Pieton> inhabitants = new ArrayList<Pieton>();
	
	public Appartement(Grid<Agent> grid, ArrayList<Point> positions, ArrayList<Point> peopleEntrances, int peopleCapacity){
		super(grid, positions, peopleEntrances);
		
		System.out.println("[INFO] New appartements created");
		
		this.canBeClose = false;
		this.peopleCapacity = peopleCapacity;
		this.openingTime = -1;
		this.closingTime = -1;	
		this.generateInhabitants();
	}
	
	public void generateInhabitants() {
		/** Generation of inhabitants **/
		for (int i = 0; i < this.peopleCapacity; i++) {
			Pieton pieton = new Pieton(this.grid);
			this.inhabitants.add(pieton);
			this.presentPeople.add(pieton);
			this.peopleTimeSpend.add(0);
		}
	}
}
