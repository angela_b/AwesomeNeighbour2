package myAwesomeNeighbour.agentsStatiques;
import java.util.*;
import java.util.stream.Collectors;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.ContextCreator.Constants;
import myAwesomeNeighbour.agentsMobiles.Pieton;
import myAwesomeNeighbour.geometrie.Point;
import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;

public abstract class Batiment extends AgentStatique {

	protected int peopleCapacity;
	protected int carsCapacity;
	protected int openingTime;
	protected int closingTime;
	protected boolean canBeClose;
	
	protected ArrayList<BatimentPart> parts = new ArrayList<>();
	protected ArrayList<BatimentPart> entrances = new ArrayList<>();
	
	protected ArrayList<Pieton> presentPeople = new ArrayList<Pieton>();
	protected ArrayList<Integer> peopleTimeSpend = new ArrayList<Integer>();
	
	protected int timeStayingBatiment = 10;
	
	protected int timesVisited = 0;
	public int getVisitedBatimentsCounts() {
		return this.timesVisited;
	}
	
	public Batiment(Grid<Agent> grid, ArrayList<Point> positions, ArrayList<Point> peopleEntrances) {
		super(grid);
		
		for (int i = 0; i < positions.size(); i++) {
			/** Verifie si le bloc que l'on souhaite ajouter est au centre ou sur un bord du batiment (pour l'affichage) **/
			int neighboursCount = 0;
			for (int j = 0; j < positions.size(); j++) {
				if (i == j) continue;				
				if (positions.get(i).nearTo(positions.get(j))) neighboursCount += 1;
			}
			
			/** Instancie le nouveau bloc **/
			BatimentPart part = new BatimentPart(this.grid, positions.get(i), this, neighboursCount == 8 ? BatimentPart.BatimentPartType.CENTER : BatimentPart.BatimentPartType.BORDER);
			
			/** Verifie si le bloc est une entrée pour pietons**/
			if (peopleEntrances.contains(positions.get(i))) {
				part.setPeopleEntrance(true);
				this.entrances.add(part);
			}
			
			/** Ajoute le bloc créé dans l'ensemble des blocs du bâtiment **/
			this.parts.add(part);
		}
	}
	
	public List<BatimentPart> getParts() { return this.parts; }
	public List<Point> getPositions() {
		return this.parts.stream().map(b -> b.getPosition()).collect(Collectors.toList());
	}
	
	public List<BatimentPart> getEntrances() { return this.entrances; }
	public List<Point> getEntrancesPositions() { 
		return this.entrances.stream().map(b -> b.getPosition()).collect(Collectors.toList()); 
	}
	
	@Override
	@ScheduledMethod(start = 1, interval = 1, priority = Constants.SCHEDULE_BATIMENT_PRIORITY)
	public void step() {
				
		for (int i = 0; i < this.peopleTimeSpend.size(); i++) {
			this.peopleTimeSpend.set(i, this.peopleTimeSpend.get(i) + 1);

			if (this.peopleTimeSpend.get(i) > this.timeStayingBatiment) {
				this.moveOutPerson(i);
			}
		}		
	}
	
	/** Methode à appeler après avoir ajouté le batiment dans le contexte, afin d'ajouter chacune de ses parties dans le contexte et dans la grille **/
	public void generateInContext(Context<Agent> context) {
		for (BatimentPart part: this.parts) {
			context.add(part);
			this.grid.moveTo(part, part.getPosition().getX(), part.getPosition().getY());
		}
	}
	
	/** Ajoute une personne dans le batiment. Cette personne est alors supprimée du contexte. **/
	public void addPerson(Pieton person) {
		this.presentPeople.add(person);
		this.peopleTimeSpend.add(0);
		Context<Agent> context = ContextUtils.getContext(this);
		context.remove(person);
	}
	
	public boolean containsPerson(Pieton person) {
		return this.presentPeople.contains(person);
	}
	
	/** Evacue une personne du batiment **/
	protected void moveOutPerson(int index) {
		Pieton person = this.presentPeople.get(index);
		this.presentPeople.remove(person);
		this.peopleTimeSpend.remove(index);
		Context<Agent> context = ContextUtils.getContext(this);
		context.add(person);
		Integer entranceIndex = RandomHelper.createUniform().nextInt() % this.entrances.size();
		this.grid.moveTo(person, this.entrances.get(entranceIndex).getPosition().getX(), this.entrances.get(entranceIndex).getPosition().getY());
		person.setPosition(new Point(this.entrances.get(entranceIndex).getPosition().getX(), this.entrances.get(entranceIndex).getPosition().getY()));
	}

	public Point getRandomEntryPosition() {
		return this.entrances.get(RandomHelper.createUniform().nextInt() % this.entrances.size()).getPosition();
	}
}
