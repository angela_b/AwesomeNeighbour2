package myAwesomeNeighbour.graphics;

import java.awt.image.BufferedImage;

import myAwesomeNeighbour.agentsStatiques.Chemin;
import myAwesomeNeighbour.agentsStatiques.Chemin.CheminDirection;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class PeopleStyle extends DefaultStyleOGL2D implements ImageTreatement {
	
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		
		if (spatial == null) {
			String path = null;
			path = new String("./icons/person.png");
			BufferedImage img = getImage(path);
			img = resizeImage(img, 25, 25);
			spatial = this.shapeFactory.createImage(path, img);
		}
		return spatial;
	}


}
