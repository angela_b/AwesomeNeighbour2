package myAwesomeNeighbour;

import myAwesomeNeighbour.ContextCreator.Constants;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;

/** Class agent schedule qui reprend les agents qui seront schédules donc agent mobile **/
public abstract class AgentSchedule extends Agent {

	public AgentSchedule(Grid<Agent> grid) {
		super(grid);
	}
	
	public abstract void step();
}
