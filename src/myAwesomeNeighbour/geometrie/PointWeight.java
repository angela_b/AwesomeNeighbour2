package myAwesomeNeighbour.geometrie;

public class PointWeight {
	public Point position;
	public int weight;
	
	
	public PointWeight(Point point, int weight) {
		this.position = point;
		this.weight = weight;
	}
	
	public PointWeight(int x, int y, int weight) {
		this.position = new Point(x, y);
		this.weight = weight;
	}
}
