package myAwesomeNeighbour.agentsMobiles;

import java.util.*;

import myAwesomeNeighbour.geometrie.*;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.ContextCreator.Constants;
import myAwesomeNeighbour.agentsStatiques.BatimentPart;
import myAwesomeNeighbour.agentsStatiques.Gare;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;

import java.util.Queue;

public class Train extends Transport {
	private ArrayList<Wagon> wagons = new ArrayList<Wagon>();
	Context<Agent> context ;
	public Queue<Point> path =  new LinkedList<Point>();
	private Queue<BatimentPart> stationsDirections = new LinkedList<BatimentPart>();
	private Wagon headWagon = null;
	public ArrayList<Pieton> travelers = new ArrayList<>();
		
	/*
	 * @param
	 * wagons : les wagons que le train contient
	 * path, le chemin que le train à parcourir
	 * */
	public Train(Grid<Agent> grid, ArrayList<Point> path, int nbwagons) {
		super(grid);
        this.path.addAll(path);
        
		for (int i = 0; i < nbwagons; i++) {
			Wagon wagon = new Wagon(this.grid, this);
			if (i == 0) { this.headWagon = wagon; }
			wagons.add(wagon);
		}
	   /*set the initial positions of wagon*/	
		for (int i = nbwagons - 1; i >= 0; i--) {
			Point location = this.path.poll();
			this.wagons.get(i).setPosition(location);
			this.path.add(location);
		}
		
		/** Instancie l'ordre des stations à visiter pour le réseau **/
        for (Point position : this.path) {
        	for (Agent agent : this.grid.getObjectsAt(position.getX(), position.getY())) {
        		if (agent instanceof BatimentPart) {
        			this.stationsDirections.add((BatimentPart) agent);
        			break;
        		}
        	}
        }
	}
	
	public void updateDirection(Network<Agent> net) {
		this.headWagon.setNextStation(this.stationsDirections.poll());
		this.stationsDirections.add(this.headWagon.nextStation);
		net.addEdge(this.headWagon, this.headWagon.nextStation);
	}
	public void updateDirection(Context<Agent> context) {
		Network<Agent> net = (Network<Agent>)context. getProjection("agent network");
		this.headWagon.setNextStation(this.stationsDirections.poll());
		this.stationsDirections.add(this.headWagon.nextStation);
		net.addEdge(this.headWagon, this.headWagon.nextStation);
	}
	
	@Override
	@ScheduledMethod(start = 1, interval = Constants.PENALITY_TRAIN, priority = Constants.SCHEDULE_TRAIN_PRIORITY)
	public void step() {
		
		Context<Object> context = ContextUtils.getContext(this); 
		Network<Agent> net = (Network<Agent>)context. getProjection("agent network");
		
		Point new_location = path.poll();
		path.add(new_location);
		for (int i = 0; i < this.wagons.size(); i++) {
			Point old_location = this.wagons.get(i).getPosition();	
			this.wagons.get(i).setPosition(new_location);
			this.grid.moveTo(this.wagons.get(i), new_location.getX(), new_location.getY());
			new_location = old_location;
		}
		
		/** Supprime la direction lorsque le train est arrivé (et appelle donc l'observeur **/
		if (this.headWagon.getPosition().equals(this.headWagon.nextStation.getPosition())) {
			BatimentPart step = this.headWagon.nextStation;
			this.updateDirection(net);; // Le fait de changer cette variable appelle l'observer
			net.removeEdge(net.getEdge(this.headWagon, step));
		}
		
		for (Pieton traveler : this.travelers) {
			int x = this.headWagon.getPosition().getX();
			int y = this.headWagon.getPosition().getY();
			this.grid.moveTo(traveler, x, y);
			traveler.setPosition(new Point(x, y));
		}
		
	}
		
	public void generateInContext(Context<Agent> context) {
		for (Wagon wagon : this.wagons) {
			context.add(wagon);
			this.grid.moveTo(wagon, wagon.getPosition().getX(), wagon.getPosition().getY());
		}
	}
	
	public void getPeopleFromGare(Gare gare) {
		ArrayList<Pieton> waitingPeople = gare.getPassengers(this);
		System.out.println("waitingPeople:" + waitingPeople.size());
		
		for (Pieton traveler : waitingPeople) {
			this.travelers.add(traveler);
			while (!traveler.listMoves.peek().equals(traveler.stationStep.getPosition())) {
				traveler.listMoves.pop();
			}
		}
	}
	
	public void removeTravelers(Gare gare) {
		
		Context<Object> context = ContextUtils.getContext(this); 
		Network<Agent> net = (Network<Agent>)context. getProjection("agent network");
		
		ArrayList<Pieton> travelersToRemove = new ArrayList<>();
		for (Pieton traveler : this.travelers) {
			if (traveler.stationStep.getBatiment() == gare) { 
				travelersToRemove.add(traveler); 
				this.grid.moveTo(traveler, traveler.stationStep.getPosition().getX(), traveler.stationStep.getPosition().getY());
				traveler.setPosition(new Point(traveler.stationStep.getPosition().getX(), traveler.stationStep.getPosition().getY()));
				traveler.refreshNextStation(net);
			}
		}
		this.travelers.removeAll(travelersToRemove);
	}
}
