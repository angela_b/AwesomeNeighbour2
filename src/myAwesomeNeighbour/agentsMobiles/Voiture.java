package myAwesomeNeighbour.agentsMobiles;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.ContextCreator.Constants;
import myAwesomeNeighbour.agentsStatiques.Chemin;
import myAwesomeNeighbour.geometrie.Point;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;
/** Classe héritant de transport et donc de ses primitives 
 * Les voitures ne sont jamais crées seules , elles appartiennt forcément à un piéton
 * @author ikuritosensei
 *
 */
public class Voiture extends Transport {
	/** Nombre de tic depuis la naissance  pour schédulé les actions **/
	// private int tic = 0;

	/** Propriétaire **/
	private Pieton master = null;

	/** Constructeur **/
	public Voiture(Grid<Agent> grid) {
		super(grid);
		System.out.println("[INFO] Nouvelle voiture");
	}

	/** Fonction schédule de agent mobile **/
	@Override
	@ScheduledMethod(start = 1, interval = Constants.PENALITY_CAR, priority = Constants.SCHEDULE_CAR_PRIORITY)
	public void step() {

		/** Récupération de la position dans la grill **/
		GridPoint gpt = grid.getLocation(this);
		Context<Object> context = ContextUtils.getContext(this);
		
		/** La voiture possède forcément un chemin à parcourir. Elle finit toujours par redevenir piéton. 
		 *  Elle se transforme en piéton si elle passe sur autre chose que de la route 
		 */

		Point dest = this.listMoves.pop();
		
		if (this.transformedIntoPedestrian(dest)) {
			this.master.transferEdgesTo(context, this.master);
			this.grid.moveTo(this.master, dest.getX(), dest.getY());
			this.master.setPosition(dest);
		} else {
			this.grid.moveTo(this, dest.getX(), dest.getY());
		}
	}

	public boolean transformedIntoPedestrian(Point position) {
		Context<Object> context = ContextUtils.getContext(this); 
		Iterable<Agent> objs = grid.getObjectsAt(position.getX(), position.getY());
		for (Agent agent : objs) {
			if (agent instanceof Chemin && ((Chemin) agent).isPavement()) {
				// Transformations en Pieton 
				context.remove(this);
				context.add(master);
				this.master.setListMoves(this.listMoves);
				return true;
			}
		}
		return false;
	}

	public Pieton getMaster() {
		return master;
	}

	public void setMaster(Pieton master) {
		this.master = master;
	}


}
