package myAwesomeNeighbour.agentsStatiques;

import myAwesomeNeighbour.Agent;
import repast.simphony.space.grid.Grid;

public class Vide extends AgentStatique {

	/** Agent utilisé uniquement pour représenter une case vide **/
	public Vide(Grid<Agent> grid) {
		super(grid);
	}
	
}
