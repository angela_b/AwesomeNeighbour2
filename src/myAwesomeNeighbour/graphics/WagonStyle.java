package myAwesomeNeighbour.graphics;

import java.awt.image.BufferedImage;

import myAwesomeNeighbour.agentsMobiles.Wagon;
import myAwesomeNeighbour.agentsStatiques.BatimentLoisir;
import myAwesomeNeighbour.agentsStatiques.BatimentPart;
import myAwesomeNeighbour.agentsStatiques.BatimentTravail;
import myAwesomeNeighbour.agentsStatiques.Gare;
import myAwesomeNeighbour.agentsStatiques.BatimentPart.BatimentPartType;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class WagonStyle extends DefaultStyleOGL2D implements ImageTreatement{

	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		String path = null;
		if (spatial == null) {
			spatial = getVSpatialWagon(spatial, (Wagon)agent);
		}
		return spatial;
		
	}
	//getting the style for wagon
	public VSpatial getVSpatialWagon(VSpatial spatial, Wagon wagon) {
		String path = null;
		path = new String("./icons/wagon.png");
		BufferedImage img = getImage(path);
		img = resizeImage(img, 20, 20);
		spatial = this.shapeFactory.createImage(path, img);
		return spatial;
	}
}
