package myAwesomeNeighbour;

import myAwesomeNeighbour.agentsStatiques.*;

import java.util.ArrayList;

import myAwesomeNeighbour.agentsStatiques.Chemin;
import myAwesomeNeighbour.agentsStatiques.Chemin.CheminType;
import myAwesomeNeighbour.geometrie.Point;
import repast.simphony.context.Context;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.Schedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.SimpleGridAdder;
import repast.simphony.space.grid.WrapAroundBorders;

/** Context creator de repast et principale fonction **/
public class ContextCreator implements ContextBuilder<Agent> {

	/** Définition des constantes de la simulation */
	public static class Constants {
		public static final int INHABITANTS_PER_HABITATIONS = RunEnvironment.getInstance().getParameters().getInteger("INHABITANTS_PER_HABITATIONS");;
		public static final int TRAINS_PER_PATH = RunEnvironment.getInstance().getParameters().getInteger("TRAINS_PER_PATH");
		
		public static final int PLANNING_SIZE = 10;
		
        public static final int PENALITY_PEDESTRIAN = 6; // RunEnvironment.getInstance().getParameters().getInteger("PENALITY_PEDESTRIAN");
        public static final int PENALITY_CAR = 3; 		 // RunEnvironment.getInstance().getParameters().getInteger("PENALITY_CAR");
        public static final int PENALITY_TRAIN = 1; 	 // RunEnvironment.getInstance().getParameters().getInteger("PENALITY_TRAIN");
        
        public static final int SCHEDULE_DEFAULT_PRIORITY = 10;
        public static final int SCHEDULE_PEDESTRIAN_PRIORITY = 1;
        public static final int SCHEDULE_CAR_PRIORITY = 1;
        public static final int SCHEDULE_TRAIN_PRIORITY = 1;
        public static final int SCHEDULE_BATIMENT_PRIORITY = 1;
        
        public static final int TIME_STAYING_BATIMENT_TRAVAIL = RunEnvironment.getInstance().getParameters().getInteger("TIME_STAYING_BATIMENT_TRAVAIL");
        public static final int TIME_STAYING_BATIMENT_LOISIR = RunEnvironment.getInstance().getParameters().getInteger("TIME_STAYING_BATIMENT_LOISIR");

    }
	
	
	@Override
	public Context build(Context<Agent> context) {
		
		PlaningGenerator.clearInstance(); // /!\ NE PAS ENLEVER. Evite des crashs lorsqu'on relance la simulation sans quitter l'interface.
		TrainsSupervisor.clearInstance();
		
		System.out.println("[INFO] Génération du terrain");
		
		context.setId("myAwesomeNeighbour");
		ContinuousSpaceFactory spaceFactory = ContinuousSpaceFactoryFinder.createContinuousSpaceFactory(null);
		
		/** Initialisation de la méthode à appeler en fin de la simulation **/
		ScheduleParameters stop = ScheduleParameters.createAtEnd(ScheduleParameters.LAST_PRIORITY);
		ISchedule sche = RunEnvironment.getInstance().getCurrentSchedule();
		sche.schedule(stop, this, "simulationStopped");
		
		/** Lancement du parser **/
		MyParser parser = new MyParser();
		parser.read("Test/testFile.txt");

		/** Deprecated **/
		int width = parser.getSizeX(); // les variables Width et Height seront chargées avec le parseur
		int height = parser.getSizeY();

		/** Creation de la grille **/
		GridFactory gridFactory = GridFactoryFinder.createGridFactory(null);
		Grid<Agent> grid = gridFactory.createGrid("grid", context, new GridBuilderParameters<Agent>(
				new WrapAroundBorders(), new SimpleGridAdder<Agent>(), true, width, height));
	
		/** Creation network **/
		NetworkBuilder <Agent > netBuilder = new NetworkBuilder<Agent >("agent network", context , true);
		netBuilder.buildNetwork();
		/** Creation de la grille de jeu a partir du fichier parser **/
		ContextCreator.createGrid(parser, grid, context);

		System.out.println("[INFO] Début de la simulation");

		return context;

	}

	/**
	 * Fonction qui une fois le fichier parse genere les objets aux bons
	 * endroits
	 **/
	public static void createGrid(MyParser parser, Grid<Agent> grid, Context<Agent> context){
		ArrayList<Chemin> paths = new ArrayList<>();
		ArrayList<Gare> gares = new ArrayList<>();
		for (int i = 0; i < parser.getSizeX(); i++) {
			for (int j = 0; j < parser.getSizeY(); j++) {
				switch (parser.getElemInGrid(i, j)) {
				case ROUTE:
					Chemin route = new Chemin(grid, new Point(i, parser.getSizeY() - 1 - j), CheminType.ROUTE, false);
					context.add(route);
					grid.moveTo(route, i, parser.getSizeY() - 1 - j);
					paths.add(route);
					break; // Route
				case RAIL:
					Chemin rail = new Chemin(grid, new Point(i, parser.getSizeY() - 1 - j), CheminType.RAIL, false);
					context.add(rail);
					grid.moveTo(rail, i, parser.getSizeY() - 1 - j);
					paths.add(rail);
					break; // Chemin de fer
				case TROTTOIR:
					Chemin trott = new Chemin(grid, new Point(i, parser.getSizeY() - 1 - j), CheminType.TROTTOIR,
							false);
					context.add(trott);
					grid.moveTo(trott, i, parser.getSizeY() - 1 - j);
					paths.add(trott);
					break;
				case HABITATION:
					for (int k = 0; k < parser.blocPositions.size(); k++) {
						if (parser.blocPositions.get(k).contains(new Point(i, parser.getSizeY() - 1 - j))) {
							Appartement appart = new Appartement(grid, parser.blocPositions.get(k),
									parser.entryPositions.get(k), Constants.INHABITANTS_PER_HABITATIONS);
							parser.blocPositions.remove(k);
							parser.entryPositions.remove(k);
							System.out.println("Add to context 1");
							context.add(appart);
							appart.generateInContext(context);
							PlaningGenerator.getInstance().addBatiment(appart);
						}
					}
					break;// Habitation
				case LOISIR:
					for (int k = 0; k < parser.blocPositions.size(); k++) {
						if (parser.blocPositions.get(k).contains(new Point(i, parser.getSizeY() - 1 - j))) {
							BatimentLoisir baloisir = new BatimentLoisir(grid, parser.blocPositions.get(k),
									parser.entryPositions.get(k));
							parser.blocPositions.remove(k);
							parser.entryPositions.remove(k);
							context.add(baloisir);
							baloisir.generateInContext(context);
							PlaningGenerator.getInstance().addBatiment(baloisir);
						}
					}

					break;// Loisir , tant que non fais ajout d'un terrain blanc
				case TRAVAIL:
					for (int k = 0; k < parser.blocPositions.size(); k++) {
						if (parser.blocPositions.get(k).contains(new Point(i, parser.getSizeY() - 1 - j))) {
							BatimentTravail batravail = new BatimentTravail(grid, parser.blocPositions.get(k),
									parser.entryPositions.get(k));
							parser.blocPositions.remove(k);
							parser.entryPositions.remove(k);
							context.add(batravail);
							batravail.generateInContext(context);
							PlaningGenerator.getInstance().addBatiment(batravail);
						}
					}

					break;// Travail
				case GARE: // On n'ajoute pas les gares dans le generateur de planning, elles servent uniquement de point de passage
					for (int k = 0; k < parser.blocPositions.size(); k++) {
						if (parser.blocPositions.get(k).contains(new Point(i, parser.getSizeY() - 1 - j))) {
							Gare gare = new Gare(grid, parser.blocPositions.get(k), parser.entryPositions.get(k));
							parser.blocPositions.remove(k);
							parser.entryPositions.remove(k);
							TrainsSupervisor.getInstance().saveStation(gare);
							context.add(gare);
							gare.generateInContext(context);
							gares.add(gare);
						}
					}
					break;
				case VIDE:
					Vide vide = new Vide(grid);
					context.add(vide);
					grid.moveTo(vide, i, parser.getSizeY() - 1 - j);
					break;
				default:
					break;
				}
			}
		}
		
		/** Fonctions à appeler une fois que tous les agents statiques soient chargés **/
		for (Chemin path : paths) { path.initDirection(); }
		for (ArrayList<Point> path : parser.listeCheminsFers) { TrainsSupervisor.getInstance().savePath(path); }
		TrainsSupervisor.getInstance().generateTrains(context, grid, Constants.TRAINS_PER_PATH);
	}
	
	public void simulationStopped() { 
		System.out.println("[INFO] End of the simulation"); 
	}
	
}
