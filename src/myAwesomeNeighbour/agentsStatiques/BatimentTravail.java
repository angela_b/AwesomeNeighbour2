package myAwesomeNeighbour.agentsStatiques;

import java.util.ArrayList;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.ContextCreator.Constants;
import myAwesomeNeighbour.geometrie.Point;
import repast.simphony.space.grid.Grid;

public class BatimentTravail extends Batiment{

	public BatimentTravail(Grid<Agent> grid, ArrayList<Point> positions, ArrayList<Point> peopleEntrances) {
		super(grid, positions, peopleEntrances);
		this.timeStayingBatiment = Constants.TIME_STAYING_BATIMENT_TRAVAIL;
	}

}
