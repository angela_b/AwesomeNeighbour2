package myAwesomeNeighbour.geometrie;

import java.util.ArrayList;

import org.apache.commons.math3.analysis.function.Abs;

public class Point {

	private int x;
	private int y;
	
	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
		
	public int getY() {
		return y;
	}
	
	public boolean equalsTo(Point c) {
		return c.getX() == x && c.getY() == y;
	}
	
	public boolean nearTo(Point c) {
		return 	   (c.getX() == this.x - 1 	&& c.getY() == this.y - 1	) 
				|| (c.getX() == this.x - 1  && c.getY() == this.y		)
				|| (c.getX() == this.x - 1  && c.getY() == this.y + 1	)
				|| (c.getX() == this.x 		&& c.getY() == this.y - 1	)
				|| (c.getX() == this.x 		&& c.getY() == this.y + 1	)
				|| (c.getX() == this.x + 1 	&& c.getY() == this.y - 1	)
				|| (c.getX() == this.x + 1 	&& c.getY() == this.y		)
				|| (c.getX() == this.x + 1 	&& c.getY() == this.y + 1	);
	
	}
	
	public ArrayList<Point> getNeighbours(int limitX, int limitY) {
		ArrayList<Point> neighbours = new ArrayList<>();
		int[] offsets = {-1, 0, 1};
		
		for (int xo : offsets) {
			for (int yo : offsets) {
				if (this.x + xo >= 0 && this.y + yo >= 0 && this.x + xo < limitX && this.y + yo < limitY && (xo == 0 || yo == 0) && !(xo == 0 && yo == 0)) {
					neighbours.add(new Point(this.x + xo, this.y + yo));
				}
			}
		}
		return neighbours;
	}
	public static ArrayList<Point> genPath(ArrayList<Point> points)
	{
		ArrayList<Point> points_generated = new ArrayList<Point>();
		points_generated.add(points.get(0));
		for(int i = 0; i < points.size() - 1; i++)
		{
		 Point ptmp = points.get(i);
		 Point ptmp2 = points.get(i + 1);
		 if (ptmp.getY() == ptmp2.getY()) {
			int signx = (ptmp2.getX() > ptmp.getX()) ? 1 : -1;
		 	for(int n = 1; n <= Math.abs(ptmp2.getX() - ptmp.getX() ); n++)
		 		points_generated.add(new Point( ptmp.getX() + signx * n, ptmp.getY()));
		 } else {
			 int signy = (ptmp2.getY() > ptmp.getY()) ? 1 : -1;
			 for(int n = 1; n <= Math.abs(ptmp2.getY() - ptmp.getY() ); n++)			 
				 points_generated.add(new Point( ptmp.getX() , ptmp.getY()+ signy * n));
		 }
		}
		return points_generated;
		
	}
	
	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}
	  @Override // Pour le arrayList.contains
	    public boolean equals(Object object)
	    {
	        boolean sameSame = false;

	        if (object != null && object instanceof Point)
	        {
	           Point comp = (Point) object;
	           return comp.getX() == x && comp.getY() == y;
	        }

	        return false;
	    }
}
