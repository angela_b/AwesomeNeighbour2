package myAwesomeNeighbour.agentsStatiques;

import java.util.ArrayList;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.ContextCreator.Constants;
import myAwesomeNeighbour.geometrie.Point;
import repast.simphony.space.grid.Grid;

public class BatimentLoisir extends Batiment {

	public BatimentLoisir(Grid<Agent> grid, ArrayList<Point> positions, ArrayList<Point> peopleEntrances ) {
		super(grid, positions, peopleEntrances);
		super.timeStayingBatiment = Constants.TIME_STAYING_BATIMENT_LOISIR;
	}

}
