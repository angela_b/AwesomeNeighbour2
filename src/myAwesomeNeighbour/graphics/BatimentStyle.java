package myAwesomeNeighbour.graphics;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import myAwesomeNeighbour.agentsStatiques.BatimentLoisir;
import myAwesomeNeighbour.agentsStatiques.BatimentPart;
import myAwesomeNeighbour.agentsStatiques.BatimentPart.BatimentPartType;
import myAwesomeNeighbour.agentsStatiques.BatimentTravail;
import myAwesomeNeighbour.agentsStatiques.Chemin;
import myAwesomeNeighbour.agentsStatiques.Gare;
import myAwesomeNeighbour.agentsStatiques.Chemin.CheminDirection;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class BatimentStyle extends DefaultStyleOGL2D implements ImageTreatement {
	//ici on load le style de chaque batiment selon son type et la partie du batiment concerné
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial){
		String path = null;
		if (spatial == null) {
			BatimentPart batpart = (BatimentPart) agent;
			if (batpart.getBatiment() instanceof Gare) {
				spatial = getVSpatialGare(spatial, batpart);
			} else if (batpart.getBatiment() instanceof BatimentTravail)
				spatial = getVSpatialTravail(spatial, batpart);
			else if (batpart.getBatiment() instanceof BatimentLoisir)
				spatial = getVSpatialLoisir(spatial, batpart);
			else
				spatial = getVSpatialGare(spatial, batpart);
		}
		return spatial;
	}
	//on load le style de gare
	public VSpatial getVSpatialGare(VSpatial spatial, BatimentPart batpart) {
		String path = null;
		if (batpart.type == BatimentPartType.CENTER)
			path = new String("./icons/centergare.png");
		else if (batpart.getPeopleEntrance())
			path = new String("./icons/door.png");
		else
			path = new String("./icons/garePart.png");
		BufferedImage img = getImage(path);
		img = resizeImage(img, 16, 16);
		spatial = this.shapeFactory.createImage(path, img);
		return spatial;
	}
	//on load le style de BatimentTravail
	public VSpatial getVSpatialTravail(VSpatial spatial, BatimentPart batpart) {
		String path = null;
		if (batpart.getPeopleEntrance() == true)
			path = new String("./icons/door.png");
		else
			path = new String("./icons/travail.png"); 
		BufferedImage img = getImage(path);
		img = resizeImage(img, 16, 16);
		spatial = this.shapeFactory.createImage(path, img);
		return spatial;
	}
	//on load le style de BatimentLoisir
	public VSpatial getVSpatialLoisir(VSpatial spatial, BatimentPart batpart) {
		String path = null;
		if (batpart.getPeopleEntrance())
			path = new String("./icons/door.png");
		else
			path = new String("./icons/shop.png");
		BufferedImage img = getImage(path);
		img = resizeImage(img, 16, 16);
		spatial = this.shapeFactory.createImage(path, img);
		return spatial;
	}
}
