package myAwesomeNeighbour;

/**
 * 
 * @author ikuritosensei
 *	Ensemble des symboles representants notre grille de facon abstraite
 *  et qui sont utilisés dans le parser
 */
/**
 * Descriptif 
 * P : Person
 * V : Voiture
 * B : Bus
 * R : Route
 * L : Loisir
 * T : Travail 
 * H : Habitation
 * C : Chemin de fer
 * W : Trottoir
 * A : Gare
 * * : Terrain
 * O : Train
 * 
 **/
public enum Symbole {
	ROUTE("Route", "R"),
	LOISIR("Loisir", "L"),
	TRAVAIL("Travail", "W"),
	HABITATION("Habitation", "H"),
	RAIL("Chemin de fer", "C"),
	TROTTOIR("Trottoir", "T"),
	VIDE("Terrain vide", "."),
	GARE("Gare", "G");
	
	private String name;
	private String symbole;
	
	Symbole(String name, String symbole)
	{
		this.name = name;
		this.symbole = symbole;
	}
	public String toString()
	{
		return name;
	}
	public String toChar() 
	{
		return symbole;
	}
}

