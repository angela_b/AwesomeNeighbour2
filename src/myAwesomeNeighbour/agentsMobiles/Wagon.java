package myAwesomeNeighbour.agentsMobiles;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.agentsStatiques.BatimentPart;
import repast.simphony.space.grid.Grid;

import myAwesomeNeighbour.geometrie.*;


public class Wagon extends Agent {
	
	public Train train;
	private Point location;
	public BatimentPart nextStation = null;

	/*
	 * mytrain : le train à qui ce wagon appartient
	 * location : la position du wagon dans la grid
	 * */
	public Wagon(Grid<Agent> grid2, Train train) {
		super(grid2);
		this.train = train;
	}
	
	public Point getPosition() {
		return this.location;
	}
	
	public void setPosition(Point location) {
		this.location = location;
	}
	
	public void setNextStation(BatimentPart nextStation) {
		this.nextStation = nextStation;
	}
}
