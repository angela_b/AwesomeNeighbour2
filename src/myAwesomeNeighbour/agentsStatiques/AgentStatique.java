package myAwesomeNeighbour.agentsStatiques;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.AgentSchedule;
import repast.simphony.space.grid.Grid;

/** 
 * Les agents statiques generent les agents dynamiques et font office de terrain vide pour l'affichage
 * @author ikuritosensei
 *
 */
public class AgentStatique extends AgentSchedule {

	public AgentStatique(Grid<Agent> grid) {
		super(grid);
	}

	@Override
	public void step() { }

}
