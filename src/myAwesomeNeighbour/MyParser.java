package myAwesomeNeighbour;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;


import myAwesomeNeighbour.geometrie.Point;

public class MyParser {
	/**
	 * [SPEC]: Aspect attendu d'un fichier texte Les X premières lignes les
	 * parametres à définir la taille en une ligne sous forme sizeX sizeY Le
	 * tableau de sizeX * sizeY elements
	 * 
	 * Les Elements sont définis dans la classe Symbole * Descriptif P :
	 * Personne V : Voiture B : Bus R : Route C : Chemin de fer L : Loisir T :
	 * Travail H : Habitation * : Terrain vide O : Train
	 * 
	 * En dessous de la map, il est nécessaire de spécifier les coordonnées des batiments et des chemins
	 * Pour un batiment, la syntaxe est la suivante:
	 * bat:[(x1,y1),...,(xn,yn)][(xe1,ye1),...,(xen,yen)]
	 * La première liste représente ses coordonnées, la seconde les positions des entrées des bâtiments
	 * Pour un chemin de fer, la syntaxe est la suivante:
	 * ch:[(x1,y1),...,(xn,yn)]
	 * Cette liste représente la position de chaque extrémités de lignes du chemin.
	 * Par exemple, [(1,1),(1,3),(3,3),(3,1),(1,1)] permettra de tracer le chemin [(1,1),(1,2),(1,3),(2,3),(3,3),(3,2),(3,1),(2,1),(1,1)]
	 *
	 * 
	 **/
	private boolean printDebug = false; // Valeur de debug actif ou non
	private int sizeX = 0;
	private int sizeY = 0;
	/**
	 * Pourquoi une grille de symbole plutot que string ? pour pouvoir evoluer
	 * si jamais on veut rajouter des traitements ou autres c'est plus facile à
	 * manipuler et maintenir.
	 **/
	private Symbole[][] grid;

	public ArrayList<ArrayList<Point>> listeCheminsFers = new ArrayList<>();
	/*
	 * liste des chemins de fer
	 */
	public ArrayList<ArrayList<Point>> blocPositions = new ArrayList<>();
	public ArrayList<ArrayList<Point>> entryPositions = new ArrayList<>();
	
	public MyParser(int X, int Y) {
		this.sizeX = X;
		this.sizeY = Y;
		grid = new Symbole[X][Y];
	}

	public MyParser() {
		// TODO
	}
	// Lire le fichier ligne par ligne
	public void read(String nameFile) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(nameFile));
			String line;
			int cpt = 0; // Compteur de la ligne courante
			while ((line = br.readLine()) != null) {
				if (printDebug)
					System.err.println("Parsing ligne " + line);
				if (cpt < 2) /**
								 * Les deux premieres lignes sont pour récupérer
								 * la taille de la grille
								 **/
				{

					if (cpt == 0)
						sizeX = Integer.parseInt(line);
					else {
						sizeY = Integer.parseInt(line);
						grid = new Symbole[sizeX][sizeY];
					}
				}
				 else {
					if (cpt - 2 < this.sizeY)
						parseLine(line, cpt - 2); // moins les deux premieres
													// lignes
					else {

						// this.listeCheminsFers.add(parsepath(line));
						parsegen(line);
					}
				}
				cpt++;
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}
    /*
     * parseur d'option pour générer les batiments et les rails
     * 
     * */
	public void parsegen(String line) {
		String[] splited = line.split("\\:+|\\;");
		String str = splited[0];
		switch (str) {
		case "ch":
			parsepath(splited[1]);
			break;
		case "bat":
			this.blocPositions.add(parseList(splited[1]));
			this.entryPositions.add(parseList(splited[2]));
			break;
		}
	}
    /*
     * parseur de list, au format [(a,b),(c,d)]
     * */
	public ArrayList<Point> parseList(String line) {
		ArrayList<Point> points_gen = new ArrayList<Point>();
		line = line.substring(1, line.length() - 1); // Get rid of braces.
		String[] parts = line.split("(?<=\\))(,\\s*)(?=\\()");
		for (String part : parts) {
			part = part.substring(1, part.length() - 1); // Get rid of parentheses.
			String[] coords = part.split(",\\s*");
			int x = Integer.parseInt(coords[0]);
			int y = Integer.parseInt(coords[1]);
			points_gen.add(new Point(x, y));
		}
		return points_gen;
	}
    /*à partir d'une liste de point, parse path*/
	public void parsepath(String line) {
		this.listeCheminsFers.add(Point.genPath(parseList(line)));
	}
	// Traiter une ligne de fichier texte
	public void parseLine(String ligne, int compteur) {
		String[] newString = ligne.split(""); // Split en caractere
		int cptChar = 0; // Compteur de charactere parcouru
		if (ligne.length() - 1 >= sizeY) {
			System.err.println("Size y invalide dans parser " + ligne.length() + " " + sizeY);
			return;
		}
		for (String c : newString) {
			if (printDebug)
				System.err.println("Analyse caractere " + c);
			switch (c) {
				/**
				 * Ajout du symbole dans la grille en x y definit par le
				 * compteur et le cptChar
				 **/
			case "R":
				setElemInGrid(Symbole.ROUTE, cptChar, compteur);
				break;// Route
			case "L":
				setElemInGrid(Symbole.LOISIR, cptChar, compteur);
				break;// Loisir
			case "W":
				setElemInGrid(Symbole.TRAVAIL, cptChar, compteur);
				break;// Travail
			case "H":
				setElemInGrid(Symbole.HABITATION, cptChar, compteur);
				break;// Habitation
			case "C":
				setElemInGrid(Symbole.RAIL, cptChar, compteur);
				break; // Chemin de fer
			case "T":
				setElemInGrid(Symbole.TROTTOIR, cptChar, compteur);
				break; // Trottoir
			case "G":
				setElemInGrid(Symbole.GARE, cptChar, compteur);
				break; // Gare
			case ".":
				setElemInGrid(Symbole.VIDE, cptChar, compteur);
				break; // Terrain vide
			default:
				System.err.println("Error invalid Line or Column: " + c);
				return;
			}
			cptChar++;
		}
	}

	public void setElemInGrid(Symbole s, int x, int y) {
		if (printDebug)
			System.err.println("Creation element " + s + " position x: " + x + " y :" + y);
		grid[x][y] = s;
	}

	public Symbole getElemInGrid(int x, int y) {
		return grid[x][y];
	}
	public int getSizeX() {
		return sizeX;
	}
	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	public int getSizeY() {
		return sizeY;
	}

	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}

	public Symbole[][] getGrid() {
		return grid;
	}

	public void setGrid(Symbole[][] grid) {
		this.grid = grid;
	}

	public void printGrid() {
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				System.out.print(grid[i][j]);
			}
			System.out.println();
		}
	}
}
