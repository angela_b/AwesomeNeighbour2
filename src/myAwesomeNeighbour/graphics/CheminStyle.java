package myAwesomeNeighbour.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import myAwesomeNeighbour.agentsStatiques.Chemin;
import myAwesomeNeighbour.agentsStatiques.*;
import myAwesomeNeighbour.agentsStatiques.Chemin.CheminDirection;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class CheminStyle extends DefaultStyleOGL2D implements ImageTreatement {
   
	@Override
	public Color getColor(Object o) {
		Chemin chemin = (Chemin) o;
		if (chemin.isRoad()) {
			return Color.DARK_GRAY;
		} else if (chemin.isPavement()) {
			return Color.GRAY;
		} else {
			return new Color(120, 60, 10);
		}
	}
	// on load le style de chaque chemin selon son type
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		
		if (spatial == null) {
			Chemin chemin = (Chemin) agent;
			if (chemin.isRoad()) {
				spatial = getVSpatialRoad(spatial, chemin);
			} else if (chemin.isPavement()) {
				spatial = getVSpatialPavement(spatial, chemin);
			} else
				spatial = getVSpatialRail(spatial, chemin);
		}
		return spatial;
	}
	//on load le style d'un chemin de type route 
	public VSpatial getVSpatialRoad(VSpatial spatial, Chemin road) {
		ArrayList<CheminDirection> directions = road.getDirection();
		String path = null;
		if (directions.contains(CheminDirection.HAUT) || directions.contains(CheminDirection.BAS))
			path = new String("./icons/roadv.png");
		else
			path = new String("./icons/roadh.png");
		BufferedImage img = getImage(path);
		img = resizeImage(img, 20, 20);
		spatial = this.shapeFactory.createImage(path, img);
		return spatial;
	}
	//on load le style d'un chemin de type troitoir
	public VSpatial getVSpatialPavement(VSpatial spatial, Chemin pavement) {
		ArrayList<CheminDirection> directions = pavement.getDirection();
		String path = null;
		path = new String("./icons/pavement2.jpg");
		BufferedImage img = getImage(path);
		img = resizeImage(img, 20, 20);
		spatial = this.shapeFactory.createImage(path, img);
		return spatial;
	}
	//on load le style d'un chemin de type rail
	public VSpatial getVSpatialRail(VSpatial spatial, Chemin rail) {

		BufferedImage img = null;
		String path = "";
		ArrayList<CheminDirection> directions = rail.getDirection();
		if (directions.contains(CheminDirection.HAUT) && directions.contains(CheminDirection.BAS)
				&& directions.contains(CheminDirection.GAUCHE) && directions.contains(CheminDirection.DROITE)) {
			img = resizeImage(getImage("./Image/rails/rail_haut_bas_gauche_droite.png"), 20, 20);
			path = "./Image/rails/rail_haut_bas_gauche_droite.png";
		} else if (directions.contains(CheminDirection.HAUT) && directions.contains(CheminDirection.BAS)
				&& directions.contains(CheminDirection.GAUCHE)) {
			img = resizeImage(getImage("./Image/rails/rail_haut_bas_gauche.png"), 20, 20);
			path = "./Image/rails/rail_haut_bas_gauche.png";
		} else if (directions.contains(CheminDirection.HAUT) && directions.contains(CheminDirection.BAS) 
				&& directions.contains(CheminDirection.DROITE)) {
			img = resizeImage(getImage("./Image/rails/rail_haut_bas_droite.png"), 20, 20);
			path = "./Image/rails/rail_haut_bas_droite.png";
		} else if (directions.contains(CheminDirection.HAUT) && directions.contains(CheminDirection.GAUCHE)
				&& directions.contains(CheminDirection.DROITE)) {
			img = resizeImage(getImage("./Image/rails/rail_haut_gauche_droite.png"), 20, 20);
			path = "./Image/rails/rail_haut_gauche_droite.png";
		} else if (directions.contains(CheminDirection.BAS) && directions.contains(CheminDirection.GAUCHE)
				&& directions.contains(CheminDirection.DROITE)) {
			img = resizeImage(getImage("./Image/rails/rail_bas_gauche_droite.png"), 20, 20);
			path = "./Image/rails/rail_bas_gauche_droite.png";
		} else if (directions.contains(CheminDirection.HAUT) && directions.contains(CheminDirection.GAUCHE)) {
			img = resizeImage(getImage("./Image/rails/rail_haut_gauche.png"), 20, 20);
			path = "./Image/rails/rail_haut_gauche.png";
		} else if (directions.contains(CheminDirection.HAUT) && directions.contains(CheminDirection.DROITE)) {
			img = resizeImage(getImage("./Image/rails/rail_haut_droite.png"), 20, 20);
			path = "./Image/rails/rail_haut_droite.png";
		} else if (directions.contains(CheminDirection.BAS) && directions.contains(CheminDirection.GAUCHE)) {
			img = resizeImage(getImage("./Image/rails/rail_bas_gauche.png"), 20, 20);
			path = "./Image/rails/rail_bas_gauche.png";
		} else if (directions.contains(CheminDirection.BAS) && directions.contains(CheminDirection.DROITE)) {
			img = resizeImage(getImage("./Image/rails/rail_bas_droite.png"), 20, 20);
			path = "./Image/rails/rail_bas_droite.png";
		} else if (directions.contains(CheminDirection.HAUT) || directions.contains(CheminDirection.BAS)) {
			img = resizeImage(getImage("./Image/rails/rail_haut_bas.png"), 20, 20);
			path = "./Image/rails/rail_haut_bas.png";
		} else if (directions.contains(CheminDirection.GAUCHE) || directions.contains(CheminDirection.DROITE)) {
			img = resizeImage(getImage("./Image/rails/rail_gauche_droite.png"), 20, 20);
			path = "./Image/rails/rail_gauche_droite.png";
		}
		
		spatial = this.shapeFactory.createImage(path, img);

		return spatial;
	}

	

}
