package myAwesomeNeighbour.agentsMobiles;


import java.util.*;
import java.util.stream.Collectors;

import cern.jet.random.Uniform;
import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.AgentSchedule;
import myAwesomeNeighbour.agentsStatiques.BatimentPart;
import myAwesomeNeighbour.agentsStatiques.Chemin;
import myAwesomeNeighbour.agentsStatiques.Chemin.CheminType;
import myAwesomeNeighbour.agentsStatiques.Gare;
import myAwesomeNeighbour.geometrie.Point;
import myAwesomeNeighbour.geometrie.PointWeight;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;

/**
 * Les agents dynamiques sont générés par les agents statiques
 * @author ikuritosensei
 *
 */
public abstract class AgentMobile extends AgentSchedule {

	/** Position **/

	protected int speed; // Vitesse de deplacement en case
	public Stack<Point> listMoves = new Stack<>(); // Pour le plus court chemin

	
	public AgentMobile(Grid<Agent> grid) {
		super(grid);
	}
	
	 /** Getter Setter **/

	public void setListMoves(Stack<Point> listMoves) {
		this.listMoves = listMoves;
	}
	
	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

}
