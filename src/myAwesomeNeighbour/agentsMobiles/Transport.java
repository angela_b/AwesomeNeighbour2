package myAwesomeNeighbour.agentsMobiles;

import myAwesomeNeighbour.Agent;
import repast.simphony.space.grid.Grid;

/** Classe definissant les moyens de transport 
 * Contenant une arraylist de pieton qu'ils transportent 
 * @author ikuritosensei
 *
 */
public abstract class Transport extends AgentMobile {

	public Transport(Grid<Agent> grid) {
		super(grid);
	}	
}
