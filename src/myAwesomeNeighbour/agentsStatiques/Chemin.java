package myAwesomeNeighbour.agentsStatiques;

import java.util.ArrayList;

import myAwesomeNeighbour.Agent;
import myAwesomeNeighbour.geometrie.Point;
import repast.simphony.space.grid.Grid;

public class Chemin extends AgentStatique {

	public enum CheminType {
		ROUTE,
		RAIL,
		TROTTOIR
	}
	
	public enum CheminDirection {
		HAUT,
		BAS,
		GAUCHE,
		DROITE
	}
	
	protected CheminType type;
	protected ArrayList<CheminDirection> directions = new ArrayList<>();
	protected Point position;
	protected boolean isPedestrianPathway = false;
	
	public Chemin(Grid<Agent> grid, Point position, CheminType type, boolean isPedestrianPathway) {
		super(grid);
		this.type = type;
		this.isPedestrianPathway = isPedestrianPathway;
		this.position = position;
	}
	
	public boolean isRoad() { return this.type == CheminType.ROUTE; }
	public boolean isRail() { return this.type == CheminType.RAIL; }
	public boolean isPavement() { return this.type == CheminType.TROTTOIR; }
	
	public void initDirection() {
		ArrayList<Point> neighbours = this.position.getNeighbours(this.grid.getDimensions().getWidth(), this.grid.getDimensions().getHeight());
		for (Point neighbour : neighbours) {
			if (Chemin.getPathType(neighbour, this.grid) == this.type && neighbour.getX() == this.position.getX() + 1) { this.directions.add(CheminDirection.DROITE); }
			if (Chemin.getPathType(neighbour, this.grid) == this.type && neighbour.getX() == this.position.getX() - 1) { this.directions.add(CheminDirection.GAUCHE); }
			if (Chemin.getPathType(neighbour, this.grid) == this.type && neighbour.getY() == this.position.getY() + 1) { this.directions.add(CheminDirection.HAUT); }
			if (Chemin.getPathType(neighbour, this.grid) == this.type && neighbour.getY() == this.position.getY() - 1) { this.directions.add(CheminDirection.BAS); }
		}
	}
	
	public CheminType getPathType() {
		return this.type;
	}
	
	public static CheminType getPathType(Point position, Grid<Agent> grid) {
		for (Object agent : grid.getObjectsAt(position.getX(), position.getY())) {
			if (agent instanceof Chemin) {
				return ((Chemin) agent).type;
			}
		}
		return null;
	}
	
	public ArrayList<CheminDirection> getDirection() {
		return this.directions;
	}
	
	public boolean isAccessibleToPedestrian() {
		return this.type == CheminType.TROTTOIR || this.isPedestrianPathway;
	}

}
